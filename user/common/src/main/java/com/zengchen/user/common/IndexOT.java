package com.zengchen.user.common;

import com.zengchen.content.common.PoemOT;
import lombok.Data;

/**
 * @program: user
 * @description: IndexOT
 * @author: zengchen
 * @create: 2019-11-01 16:39
 **/
@Data
public class IndexOT {
    private String today;
    private Integer stayDays;
    private PoemOT poem;
}
