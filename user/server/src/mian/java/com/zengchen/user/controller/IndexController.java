package com.zengchen.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zengchen.common.bean.ResponseVO;
import com.zengchen.common.utils.JWTUtils;
import com.zengchen.content.common.PoemOT;
import com.zengchen.user.common.IndexOT;
import com.zengchen.user.config.JwtConfig;
import com.zengchen.user.entity.CurrentPoem;
import com.zengchen.user.feignclient.ContentCenterFeignClient;
import com.zengchen.user.service.CurrentPoemService;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * <p>
 *  首页controller
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
@RestController
@Slf4j
//@RefreshScope
public class IndexController {

    @Autowired
    private CurrentPoemService currentPoemService;

    @Autowired
    private ContentCenterFeignClient contentCenterFeignClient;

    @Autowired
    private JwtConfig jwtConfig;

    @GetMapping("/index")
    public Object index(HttpServletRequest request) {
        String token = request.getParameter("token");
        log.info("index start with token = {}",token);
        IndexOT indexOT = new IndexOT();
        // 1.检查是否登录
        Boolean isLogin = StringUtils.isBlank(token) ? false : JWTUtils.validateToken(token, jwtConfig.getSecret());
        if (!isLogin) {
            log.info("未登录！");
            indexOT.setStayDays(0);
            indexOT.setToday(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            // feign 请求 content 微服务 获取排在第一位的诗词
            // 随机发放一首诗词
            List<PoemOT> allPoem = contentCenterFeignClient.getAllPoem();
            PoemOT poemOT = allPoem.get(ThreadLocalRandom.current().nextInt(allPoem.size()));
            PoemOT poem = contentCenterFeignClient.getPoem(poemOT.getId());
            indexOT.setPoem(poem);
            log.info("index end with indexOT = {}",indexOT);
            return ResponseVO.success(indexOT);
        }

        Claims claimsFromToken = JWTUtils.getClaimsFromToken(token, jwtConfig.getSecret());
        Integer memberId = (Integer) claimsFromToken.get("memberId");
        log.info("已登录！memberId = {}",memberId);

        CurrentPoem currentPoem = currentPoemService.getOne(new QueryWrapper<CurrentPoem>().eq("member_id", memberId));
        indexOT.setStayDays(currentPoem.getStayDays());
        indexOT.setToday(currentPoem.getHappenedDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        // feign 请求 content 微服务 根据诗词 id 获取诗词
        PoemOT poemOT = contentCenterFeignClient.getPoem(currentPoem.getPoemId());
        indexOT.setPoem(poemOT);
        log.info("index end with indexOT = {}",indexOT);
        return ResponseVO.success(indexOT);
    }



}

