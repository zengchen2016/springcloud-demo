package com.zengchen.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zengchen.user.entity.CurrentPoem;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zengchen123
 * @since 2019-11-04
 */
public interface CurrentPoemMapper extends BaseMapper<CurrentPoem> {

}
