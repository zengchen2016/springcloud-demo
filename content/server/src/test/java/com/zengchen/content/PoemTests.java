package com.zengchen.content;

import com.zengchen.content.entity.Poem;
import com.zengchen.content.mapper.PoemMapper;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.sql.rowset.serial.SerialBlob;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Date;

@RunWith(SpringRunner.class)
@MapperScan("com.zengchen.content.mapper")
@SpringBootTest
public class PoemTests {

    @Resource
    PoemMapper poemMapper;

    @Test
    public void mybatisCURD() throws IOException, SQLException {

        byte[] bytes = IOUtils.toByteArray(new FileInputStream("d:\\gitee\\springcloud\\mp3\\登高.mp3"));
        Blob blob = new SerialBlob(bytes);
        Poem poem = Poem.builder()
                .title("登高")
                .author("杜甫")
                .content("<p style=\"text-align:center;\"><strong><span style=\"font-size:32px;font-family:宋体, simsun;\">登 高</span></strong></p><p style=\"text-align:center;\"><span style=\"font-family:宋体, simsun;\"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 杜甫·唐</span></p><p style=\"text-align:center;\"><span><br /></span></p><p style=\"text-align:center;line-height:3em;\"><strong><span style=\"font-size:20px;font-family:宋体, simsun;\">风急天高猿啸哀，渚清沙白鸟飞回。</span></strong></p><p style=\"text-align:center;line-height:3em;\"><strong><span style=\"font-size:20px;font-family:宋体, simsun;\">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style=\"text-align:center;line-height:3em;\"><strong><span style=\"font-size:20px;font-family:宋体, simsun;\">万里悲秋常作客，百年多病独登台。</span></strong></p><p style=\"text-align:center;line-height:3em;\"><strong><span style=\"font-size:20px;font-family:宋体, simsun;\">艰难苦恨繁霜鬓，潦倒新停浊酒杯。</span></strong></p><p><br /></p>")
                .createTime(new Date())
                .updateTime(new Date())
                .type("1")
                .build();
//        System.out.println(poem);
        poemMapper.insert(poem);

        // 初始化 影响行数
//        int result = 0;
//        // 初始化 User 对象
//        User user = new User();
//
//        // 插入 User (插入成功会自动回写主键到实体类)
//        user.setName("Tom");
//        result = userMapper.insert(user);
//
//        // 更新 User
//        user.setPassword(18+"");
//        result = userMapper.updateById(user);
//
//        // 查询 Userd
//        User exampleUser = userMapper.selectById(user.getId());

        // 查询姓名为‘张三’的所有用户记录
//        List<User> userList = userMapper.selectList(
//                new QueryWrapper<User>().eq("name", "Tom")
//        );
//
//        // 删除 User
//        result = userMapper.deleteById(user.getId());
//        List<Poem> userList = poemMapper.selectList(new QueryWrapper<Poem>());
//        System.out.println(userList.size());
    }

}
