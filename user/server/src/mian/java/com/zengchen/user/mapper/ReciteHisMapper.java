package com.zengchen.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zengchen.user.common.ReciteHisOT;
import com.zengchen.user.entity.ReciteHis;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
public interface ReciteHisMapper extends BaseMapper<ReciteHis> {

    List<ReciteHisOT> queryReciteHisPage(Page page);

    @Select( "SELECT p.id poemId,rh.view_date,rh.recite_date,rh.last_days,p.dynasty,p.title " +
            " FROM user.reciteHis rh LEFT JOIN content.poem p ON rh.poem_id = p.id" +
            " WHERE rh.member_id = ${memberId} AND rh.recite_date IS NOT NULL AND rh.recite_date != '' " +
            " ORDER BY rh.recite_date DESC")
    List<ReciteHisOT> queryReciteHisPage(Page<ReciteHisOT> page,@Param(value = "memberId") String  memberId);
}
