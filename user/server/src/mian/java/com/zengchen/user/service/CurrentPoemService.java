package com.zengchen.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zengchen.user.entity.CurrentPoem;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zengchen123
 * @since 2019-11-04
 */
public interface CurrentPoemService extends IService<CurrentPoem> {

}
