package com.zengchen.content.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zengchen.content.entity.Dynasty;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-03
 */
public interface DynastyMapper extends BaseMapper<Dynasty> {

}
