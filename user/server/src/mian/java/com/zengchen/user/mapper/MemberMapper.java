package com.zengchen.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zengchen.user.entity.Member;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
public interface MemberMapper extends BaseMapper<Member> {

    public List<String> selectTest();
}
