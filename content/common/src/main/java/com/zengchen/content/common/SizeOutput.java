package com.zengchen.content.common;

import lombok.Data;

@Data
public class SizeOutput {

    private Integer id;

    private String name;
}
