package com.zengchen.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zengchen.user.common.UserInfoOT;
import com.zengchen.user.entity.CurrentPoem;
import com.zengchen.user.entity.Member;
import com.zengchen.user.entity.ReciteHis;
import com.zengchen.user.mapper.MemberMapper;
import com.zengchen.user.service.CurrentPoemService;
import com.zengchen.user.service.MemberService;
import com.zengchen.user.service.ReciteHisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
@Service
@Slf4j
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {

    @Autowired
    private ReciteHisService reciteHisService;

    @Autowired
    private CurrentPoemService currentPoemService;

    @Override
    public List<String> selectTest() {
        return this.baseMapper.selectTest();
    }

    @Override
    public UserInfoOT getMyPageUserInfoOT(Member member) {
        log.info("getMyPageUserInfoOT start memberId = {}", member.getId());
        UserInfoOT userInfoOT = new UserInfoOT();
        Integer memberId = member.getId();
        userInfoOT.setUserId(memberId);
        userInfoOT.setUserName(member.getNickname());
        userInfoOT.setHeadUrl(member.getHeadUrl());
        userInfoOT.setPoints(member.getPoints());

        // 已背诵总数
        int reciteTotal = reciteHisService.count(new QueryWrapper<ReciteHis>().eq("member_id", memberId));
        userInfoOT.setAlreadyRecite(reciteTotal);

        // 当前诗词停留天数
        CurrentPoem currentPoem = currentPoemService.getOne(new QueryWrapper<CurrentPoem>().eq("member_id", memberId));
        userInfoOT.setStayDays(currentPoem.getStayDays());

        // 注册天数
        long stayDays = LocalDate.now().toEpochDay() - member.getCreateTime().toLocalDate().toEpochDay() + 1;
        userInfoOT.setRegisterDays(stayDays);

        log.info("getMyPageUserInfoOT end with userInfoOT = {}", userInfoOT);
        return userInfoOT;
    }
}
