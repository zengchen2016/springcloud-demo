package com.zengchen.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
@TableName("reciteHis")
@Data
public class ReciteHis implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户ID
     */
    private Integer memberId;
    /**
     * 诗词ID
     */
    private Integer poemId;
    /**
     * 派送诗词日期
     */
    private String viewDate;

    private String author;

    private String title;

    private String dynasty;
    /**
     * 会背诵日期
     */
    private String reciteDate;
    /**
     * 与上次背诵间隔的天数
     */
    private Integer stayDays;

    @TableField("createTime")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date createTime;

    @TableField("updateTime")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date updateTime;

}
