package com.zengchen.user.common;

import lombok.Getter;

/**
 * @program: user
 * @description: PointLogType
 * @author: zengchen
 * @create: 2019-11-06 10:49
 **/
@Getter
public enum PointLogType {

    REGISTER("1","注册赠送", 10);

    private String type;
    private String remarks;
    private Integer amount;
    PointLogType(String type, String remarks, Integer amount){
        this.type = type;
        this.remarks = remarks;
        this.amount = amount;
    }
}
