package com.zengchen.content.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author zengchen123
 * @since 2019-11-04
 */
@Data
@Builder
public class Poem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 标题
     */
    private String title;
    /**
     * 朝代
     */
    private String dynasty;
    /**
     * 作者
     */
    private String author;
    /**
     * 内容
     */
    private String content;

    /**
     * 去掉诗名作者的纯汉字内容
     */
    private String onlyContent;
    /**
     * 译文
     */
    private String appreciation1;
    /**
     * 注释
     */
    private String appreciation2;
    /**
     * 赏析
     */
    private String appreciation3;
    /**
     * 发音链接
     */
    private String readUrl;
    /**
     * 1：诗
     2：词
     3：古文
     */
    private String type;
    /**
     * 音频，最大4G
     */
    private String mp3;

    @TableField("createTime")
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date createTime;

    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    @TableField("updateTime")
    private Date updateTime;
    /**
     * 发放顺序
     */
    private Integer orders;

}
