package com.zengchen.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author zengchen123
 * @since 2019-11-04
 */
@TableName("currentPoem")
@Data
public class CurrentPoem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户ID
     */
    private Integer memberId;
    /**
     * 诗词
     */
    private Integer poemId;
    /**
     * 发放日期
     */
    private LocalDateTime happenedDate;
    /**
     * 停滞天数
     */
    private Integer stayDays;

    private String status;

    private LocalDate updateDate;

}
