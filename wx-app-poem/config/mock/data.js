var indexData = {
  "today":"2019-09-19",
  "intervalDays":4,
  "poem": {
    "id": 1,
    "title":"登高",
    "author":"杜甫",
    "dynasty":"唐",
    "mp3":"denggao.mp3",
    "content":'<p style="text-align:center;"><strong><span style="font-size:32px;font-family:宋体, simsun;">登 高</span></strong></p><p style="text-align:center;"><span style="font-family:宋体, simsun;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 杜甫·唐</span></p><p style="text-align:center;"><span><br /></span></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">风急天高猿啸哀，渚清沙白鸟飞回。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">无边落木萧萧下，不尽长江滚滚来。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">万里悲秋常作客，百年多病独登台。</span></strong></p><p style="text-align:center;line-height:3em;"><strong><span style="font-size:18px;font-family:宋体, simsun;">艰难苦恨繁霜鬓，潦倒新停浊酒杯。</span></strong></p><p><br /></p>'
  }
}

var myData = {
  "code":0,
  "msg": "ok",
  "data": {
    "userInfoOT": {
      "token": "123521312-ddeewxee-cafafaffa",
      "userId": 1,
      "userName": "一粒尘埃",             "headUrl":"https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJlmg4VyzgVKREibx2oR4zlsw5nicpLmiaTE4EXTbriauG7aFmUibWEHqkjSribjYu4zEICrg37FkVxbo0w/132",
      "alreadyRecite": 101,
      "stayDays":10,
      "registerDays":190
    }
  }
}

module.exports = {
  indexData: indexData,
  myData: myData
}