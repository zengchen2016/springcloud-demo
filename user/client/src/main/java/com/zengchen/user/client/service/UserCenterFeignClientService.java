package com.zengchen.user.client.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zengchen.user.common.ReciteHisOT;
import org.springframework.web.bind.annotation.GetMapping;

public interface UserCenterFeignClientService {

    @GetMapping(value = "/reciteHis/testAno")
    Page<ReciteHisOT> memberRctHisAno();
}
