package com.zengchen.user.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
//@RefreshScope
public class JwtConfig {

    /**
     * token 密钥
     */
    @Value("${jwt.secret}")
    public String secret;

    /**
     * token 失效时间，单位秒
     */
    @Value("${jwt.expirationTimeInSecond}")
    public String expirationTimeInSecond;

}
