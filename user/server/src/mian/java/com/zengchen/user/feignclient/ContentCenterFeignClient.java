package com.zengchen.user.feignclient;

import com.zengchen.content.common.PoemOT;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @program: user
 * @description: ContentCenterFeignClient
 * @author: zengchen
 * @create: 2019-11-04 16:58
 **/
@FeignClient(name="content-center")
public interface ContentCenterFeignClient {

    @GetMapping(value = "/poem/getPoemByOrder")
    PoemOT getPoemByOrder(@RequestParam("order") Integer order);

    @GetMapping(value = "/poem/getNextPoemId")
    Integer getNextPoemId(@RequestParam("id") Integer id);

    @GetMapping(value = "/poem/{id}")
    PoemOT getPoem(@PathVariable Integer id);

    @GetMapping(value = "/poem/getAllPoem")
    List<PoemOT> getAllPoem();
}
