package com.zengchen.content.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;

//@Configuration
public class UserCenterFeignClientConfiguration {

    @Bean
    public Logger.Level loggerLevel(){
        //全日志
        return Logger.Level.FULL;
    }
}
