package com.zengchen.content.common;/**
 * @program: content
 * @description: Test
 * @author: zengchen
 * @create: 2019-09-13 07:43
 **/

/**
 * @program: content
 * @description: Test
 * @author: zengchen
 * @create: 2019-09-13 07:43
 **/
public class Test {

    public static void main(String[] args) throws ClassNotFoundException {
        Class clazz = Class.forName("com.zengchen.content.common.SizeOutput");
        System.out.println("=====getConstructors()==============");
        for (int i = 0; i < clazz.getConstructors().length; i++) {
            System.out.println(clazz.getConstructors()[i]);
        }
        System.out.println("=====getDeclaredConstructors()==============");
        for (int i = 0; i < clazz.getDeclaredConstructors().length; i++) {
            System.out.println(clazz.getDeclaredConstructors()[i]);
        }
        System.out.println("=====getFields()==============");
        for (int i = 0; i < clazz.getFields().length; i++) {
            System.out.println(clazz.getFields()[i]);
        }
        System.out.println("=====getDeclaredFields()==============");
        for (int i = 0; i < clazz.getDeclaredFields().length; i++) {
            System.out.println(clazz.getDeclaredFields()[i]);
        }
        System.out.println("=====getMethods()==============");
        for (int i = 0; i < clazz.getMethods().length; i++) {
            System.out.println(clazz.getMethods()[i]);
        }

        System.out.println("=====getDeclaredMethods()==============");
        for (int i = 0; i < clazz.getDeclaredMethods().length; i++) {
            System.out.println(clazz.getDeclaredMethods()[i]);
        }
    }
}
