package com.zengchen.common.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseCode {

    ERROR_SYS(9999,"系统异常"),
    ERROR_ALREADY_RECITE(4,"已经背诵过了"),
    ERROR_LOGGED_OUT(3,"用户未登录"),
    ERROR_LOGIN(2,"登录异常"),
    ERROR_PARAMS(1,"参数错误");

    private Integer code;
    private String msg;

}
