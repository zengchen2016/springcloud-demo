import common from '../../config/js/common'
var currentPage

Page({

  /**
   * 页面的初始数据
   */
  data: {
    current: 1,
    size: 15,
    listData: [
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let Pages = getCurrentPages()
    currentPage = Pages[Pages.length - 1]

    this.getPointLog(function (dataPage) {
      currentPage.setData({
        listData: dataPage.records,
        current: dataPage.current + 1
      })
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getPointLog(function (dataPage) {
      let newList = currentPage.data.listData.concat(dataPage.records)
      currentPage.setData({
        listData: newList,
        current: dataPage.current + 1
      })
    })
  },
  getPointLog: function (successFunc) {
    let token = wx.getStorageSync('token')
    if (token) {
      wx.request({
        url: common.user_center + "pointLog/getPointLog",
        method: 'GET',
        header: {
          'WX-TOKEN': token
        },
        data: {
          "token": token,
          'current': currentPage.data.current,
          'size': currentPage.data.size
        },
        success(res) {
          let data = res.data;
          console.log("请求积分记录 后台返回")
          console.log(data)
          if (data.code == 0) {// 服务端调用成功
            let dataPage = data.data.dataPage
            successFunc(dataPage)
          } else if (data.code == 3) { // 未登录
            wx.reLaunch({
              url: '/pages/my/my'
            })
          }
        },
        fail(res) {
          console.log("请求积分记录 服务调用异常：" + res)
        },
      })
    }
  }
})