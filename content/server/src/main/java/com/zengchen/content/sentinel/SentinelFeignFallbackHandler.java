package com.zengchen.content.sentinel;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zengchen.content.common.PoemOT;
import com.zengchen.content.feignclient.UserCenterFeignClient;
import com.zengchen.user.common.CurrentPoemOT;
import com.zengchen.user.common.ReciteHisOT;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SentinelFeignFallbackHandler implements UserCenterFeignClient {
    @Override
    public Page<ReciteHisOT> memberRctHisAno() {
        log.warn("memberRctHisAno fallback 了");
        return null;
    }

    @Override
    public ReciteHisOT getReciteHis(Integer poemId) {
        log.warn("getReciteHis fallback 了");
        return null;
    }

    @Override
    public void insertReciteHis(PoemOT poemOT) {
        log.warn("insertReciteHis fallback 了");
    }

    @Override
    public void updateCurrentPoem(CurrentPoemOT currentPoemOT) {
        log.warn("updateCurrentPoem fallback 了");
    }
}
