package com.zengchen.content.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zengchen.content.entity.Poem;
import com.zengchen.content.mapper.PoemMapper;
import com.zengchen.content.service.PoemService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-03
 */
@Service
public class PoemServiceImpl extends ServiceImpl<PoemMapper, Poem> implements PoemService {

}
