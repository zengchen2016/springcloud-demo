package com.zengchen.content.config.ribbonrule;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.DynamicServerListLoadBalancer;
import com.netflix.loadbalancer.Server;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.alibaba.nacos.NacosDiscoveryProperties;
import org.springframework.cloud.alibaba.nacos.ribbon.NacosServer;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 同集群同版本权重负载规则
 * </p>
 *
 * @author zengchen
 * @since 2019-08-11
 */
@Slf4j
public class NacosClusterMetadataWeightedRule extends AbstractLoadBalancerRule {

    @Autowired
    private NacosDiscoveryProperties discoveryProperties;

    // 自己的版本号
    private final static String SELF_VERSION = "self-version";
    // 需要调用的版本号
    private final static String TARGET_VERSION = "target-version";

    private Server choose(DynamicServerListLoadBalancer lb, Object key) {
        if (lb == null) {
            return null;
        }
        try {
            // 本服务目标版本号
            String targetVersion = discoveryProperties.getMetadata().get(SELF_VERSION);
            // 本服务所属集群
            String clusterName = discoveryProperties.getClusterName();
            // 要请求的服务的名称
            String targetInstanceName = lb.getName();
            // 服务相关Api
            NamingService namingService = discoveryProperties.namingServiceInstance();
            // 获取目标服务所有实例(健康的实例)
            List<Instance> instances = namingService.selectInstances(targetInstanceName, true);
            List<Instance> versionMatchInstances = instances;
            // 获得targetVersion的实例
            if(StringUtils.isNotBlank(targetVersion)){
                versionMatchInstances = instances.stream()
                        .filter(instance -> {
                            return Objects.equals(instance.getMetadata().get(SELF_VERSION),targetVersion);
                        }).collect(Collectors.toList());
                if(CollectionUtils.isEmpty(versionMatchInstances)){
                    log.warn("未找到元数据匹配的目标实例！请检查配置。targetVersion = {}, instance = {}", targetVersion, instances);
                    return null;
                }
            }

            // 从元数据匹配的实例中获得同集群实例
            List<Instance> clusterMetadataMatchInstances = versionMatchInstances;
            if(StringUtils.isNotBlank(clusterName)){
                clusterMetadataMatchInstances =  versionMatchInstances.stream()
                        .filter(instance -> {
                            return Objects.equals(instance.getClusterName(), clusterName);
                        }).collect(Collectors.toList());
                if(CollectionUtils.isEmpty(clusterMetadataMatchInstances)){
                    log.warn("发生跨集群调用。clusterName = {}, targetVersion = {}, clusterMetadataMatchInstances = {}", clusterName, targetVersion, clusterMetadataMatchInstances);
                    clusterMetadataMatchInstances = versionMatchInstances;
                }
            }
            // 根据权重从toBeChosenInstances选择一个实例
            return new NacosServer(ExtendsBalancer.getInstanceByWeight(clusterMetadataMatchInstances));
        } catch (NacosException e) {
            log.error("元数据同集群负载规则发生异常", e);
        }
        return null;
    }

    @Override
    public Server choose(Object key) {
        log.info("负载均衡规则：NacosClusterMetadataWeightedRule");
        return choose((DynamicServerListLoadBalancer) getLoadBalancer(), key);
    }

    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {

    }
}

