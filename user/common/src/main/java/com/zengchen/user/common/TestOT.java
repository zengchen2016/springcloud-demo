package com.zengchen.user.common;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
public class TestOT implements Serializable {

    private static final long serialVersionUID = 1L;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "TestOT{" +
                "title='" + title + '\'' +
                '}';
    }
}
