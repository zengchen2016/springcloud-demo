import common from '../../config/js/common';

var currentPage
Page({

  /**
   * 页面的初始数据
   */
  data: {
    poem: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (params) {
    let Pages = getCurrentPages()
    currentPage = Pages[Pages.length - 1]

    let poemId = params.poemId
    if (poemId) {
      wx.request({
        url: common.content_center + "poem/" + poemId,
        method: 'GET',
        success(res) {
          let data = res.data;
          currentPage.setData({
            poem: data,
          })
        },
        fail(res) {
          console.log("请求诗词 服务调用异常：" + res)
        },
      })
    }
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})