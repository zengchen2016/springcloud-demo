package com.zengchen.user.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class WXConfig {

    @Value("${wx.appid}")
    public String appid;

    @Value("${wx.secret}")
    public String secret;

    @Value("${wx.code2Session_url}")
    public String code2Session_url;

}
