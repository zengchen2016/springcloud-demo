package com.zengchen.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zengchen.user.entity.PointLog;
import com.zengchen.user.mapper.PointLogMapper;
import com.zengchen.user.service.PointLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zengchen123
 * @since 2019-11-02
 */
@Service
public class PointLogServiceImpl extends ServiceImpl<PointLogMapper, PointLog> implements PointLogService {

}
