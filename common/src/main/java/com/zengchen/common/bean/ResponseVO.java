package com.zengchen.common.bean;

import com.zengchen.common.enums.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * http请求返回的最外层对象
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseVO<T> {

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 具体内容
     */
    private T data;

    public static ResponseVO success(Object data){
        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(0);
        responseVO.setData(data);
        responseVO.setMsg("OK");
        return responseVO;
    }

    public static ResponseVO fail(Integer code,String msg){
        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(code);
        responseVO.setMsg(msg);
        return responseVO;
    }

    public static ResponseVO fail(ResponseCode responseCode){
        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(responseCode.getCode());
        responseVO.setMsg(responseCode.getMsg());
        return responseVO;
    }

}
