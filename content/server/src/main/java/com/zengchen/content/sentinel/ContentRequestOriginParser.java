package com.zengchen.content.sentinel;

import com.alibaba.csp.sentinel.adapter.servlet.callback.RequestOriginParser;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 提供来源
 */
//@Component
public class ContentRequestOriginParser implements RequestOriginParser {
    @Override
    public String parseOrigin(HttpServletRequest httpServletRequest) {
//        String origin = httpServletRequest.getHeader("origin");
        String origin = httpServletRequest.getParameter("origin"); // 放在参数中方便测试
        if(StringUtils.isBlank(origin)){
            throw new IllegalArgumentException("origin is blank!");
        }
        return origin;
    }
}
