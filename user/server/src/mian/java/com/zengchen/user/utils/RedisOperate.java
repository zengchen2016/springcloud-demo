package com.zengchen.user.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class RedisOperate {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // ************************
    // value 是Object的操作方法
    // ************************
    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public void set(String key, Object value, long timeout, TimeUnit unit) {
        redisTemplate.opsForValue().set(key, value, timeout, unit);
    }

    /**
     * 如果key不存在，设置并返回true
     * 如果key已存在，不设置并返回false
     *
     * @param key
     * @param value
     * @return
     */
    public Boolean setIfAbsent(String key, Object value) {
        return redisTemplate.opsForValue().setIfAbsent(key, value);
    }

    public Object getObject(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 此方法不同模版设置的key可以共用，所以只定义一个就行了
     * @param key
     * @return
     */
    public Long size(String key) {
        return redisTemplate.opsForValue().size(key);
    }

    /**
     * 递增并且返回递增之后的值
     * @param key
     * @param delta
     * @return
     */
    public Long increment(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子delta必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    public Double increment(String key, double delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子delta必须大于0");
        }
        return redisTemplate.opsForValue().increment(key, delta);
    }

    public Boolean expire(String key,long timeout, TimeUnit unit) {
        if(timeout > 0){
            return redisTemplate.expire(key,timeout,unit);
        }
       return false;
    }

    public Long getExpire(String key, TimeUnit unit) {
        if(null == unit){
            return redisTemplate.getExpire(key);
        }
        return redisTemplate.getExpire(key,unit);
    }

    public void delete(String key) {
        redisTemplate.delete(key);
    }


    /**
     * 设置键的字符串值并返回其旧值
     *
     * @param key
     * @param value 新值
     * @return 旧值
     */
    public Object getAndSet(String key, Object value) {
        return redisTemplate.opsForValue().getAndSet(key, value);
    }

    // ************************
    // value 是字符串的操作方法
    // ************************
    public void set(String key, String value) {
        stringRedisTemplate.opsForValue().set(key, value);
    }

    public void set(String key, String value, long timeout, TimeUnit unit) {
        stringRedisTemplate.opsForValue().set(key, value, timeout, unit);
    }

    public void set(String key, String value, long offset) {
        stringRedisTemplate.opsForValue().set(key, value, offset);
    }

    public String getString(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    public Boolean setIfAbsent(String key, String value) {
        return stringRedisTemplate.opsForValue().setIfAbsent(key, value);
    }

    public String getAndSet(String key, String value) {
        return stringRedisTemplate.opsForValue().getAndSet(key, value);
    }

}