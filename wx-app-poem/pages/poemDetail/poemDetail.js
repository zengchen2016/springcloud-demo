import common from '../../config/js/common';

var currentPage
var innerAudioContext

Page({
  data: {
    imageSrc: "/image/laba.png",
    status: "0",
    poem:{}
  },
  onLoad: function (params) {
    let Pages = getCurrentPages()
    currentPage = Pages[Pages.length - 1]

    let poemId = params.poemId
    if (poemId) {
      wx.request({
        url: common.content_center + "poem/" + poemId,
        method: 'GET',
        success(res) {
          let data = res.data;
          console.log("请求诗词 后台返回")

          innerAudioContext = wx.createInnerAudioContext()
          innerAudioContext.autoplay = false
          let mp3src = common.mp3_url + data.mp3
          innerAudioContext.src = mp3src

          currentPage.setData({
            poem: data,
            status: "0"
          })
        },
        fail(res) {
          console.log("请求诗词 服务调用异常：" + res)
        },
      })
    }
  },
  onHide: function(){
    // console.log("onHide")
    innerAudioContext.destroy()
    this.setData({
      imageSrc: "/image/laba.png",
      status: "0"
    })
  },
  onUnload: function(){
    // console.log("onUnload")
    innerAudioContext.destroy()
    this.setData({
      imageSrc: "/image/laba.png",
      status: "0"
    })
  },
  learnRead: function (event) {
    let status = event.currentTarget.dataset.status
    if (status == "0"){ // 停止状态
      this.setData({
        imageSrc: "/image/zanting.png",
        status: "1"
      })
      innerAudioContext.play()
    } else if (status == "1"){
      this.setData({
        imageSrc: "/image/laba.png",
        status: "0"
      })
      innerAudioContext.pause()
    }
  }
})