package com.zengchen.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zengchen.user.entity.PointLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zengchen123
 * @since 2019-11-02
 */
public interface PointLogService extends IService<PointLog> {

}
