//package com.zengchen.user;
//
//import com.zengchen.user.utils.RedisOperate;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.concurrent.TimeUnit;
//
///**
// * @program: user
// * @description: com.zengchen.user.RedisTest
// * @author: zengchen
// * @create: 2019-11-10 18:04
// **/
//@RunWith(SpringRunner.class)
//@MapperScan("com.zengchen.user.mapper")
//@SpringBootTest
//public class RedisTest {
//
//    @Autowired
//    RedisOperate redisOperate;
//
//    @Autowired
//    private StringRedisTemplate stringRedisTemplate;
//
//    @Test
//    public void set() {
//        for (int i = 0; i < 10; i++) {
//            System.out.println(stringRedisTemplate.opsForValue().setIfAbsent("CurrentPoemTask","CurrentPoemTask",60, TimeUnit.SECONDS));
//        }
//
//    }
//}
