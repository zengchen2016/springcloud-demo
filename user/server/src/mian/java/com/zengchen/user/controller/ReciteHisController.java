package com.zengchen.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zengchen.common.bean.ResponseVO;
import com.zengchen.common.enums.ResponseCode;
import com.zengchen.content.common.PoemOT;
import com.zengchen.user.auth.CheckLogin;
import com.zengchen.user.common.ReciteHisOT;
import com.zengchen.user.entity.CurrentPoem;
import com.zengchen.user.entity.ReciteHis;
import com.zengchen.user.service.CurrentPoemService;
import com.zengchen.user.service.ReciteHisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
@RestController
@RequestMapping("reciteHis")
@Slf4j
public class ReciteHisController {


    @Autowired
    private ReciteHisService reciteHisService;

    @Autowired
    private CurrentPoemService currentPoemService;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @GetMapping("getReciteHisByPage")
    @CheckLogin
    public Object getReciteHisByPage(@ModelAttribute Page<ReciteHis> page, HttpServletRequest request) {
        Map<String, Object> responseData = new HashMap<>();
        try {
            Integer memberId = (Integer) request.getAttribute("memberId");
            log.info("getReciteHisByPage with current={},size={},memberId={}", page.getCurrent(), page.getSize(), memberId);

            IPage<ReciteHis> dataPage = reciteHisService.page(page, new QueryWrapper<ReciteHis>()
                    .eq("member_id", memberId)
                    .orderByDesc("recite_date")
            );
            responseData.put("dataPage", dataPage);
            log.info("getReciteHisByPage end with success!");
            return ResponseVO.success(responseData);
        } catch (Exception e) {
            log.error("getReciteHisByPage end with Exception", e);
            return ResponseVO.fail(ResponseCode.ERROR_SYS);
        }

    }

    @GetMapping("getReciteHis")
    @CheckLogin
    public ReciteHisOT getReciteHis(Integer poemId, HttpServletRequest request) {
        Integer memberId = (Integer) request.getAttribute("memberId");
        log.info("getReciteHis with poemId={},memberId={}", poemId, memberId);

        ReciteHis reciteHis =  reciteHisService.getOne(new QueryWrapper<ReciteHis>().eq("member_id", memberId)
        .eq("poem_id", poemId));

        if(reciteHis == null){
            return null;
        }
        ReciteHisOT reciteHisOT = new ReciteHisOT();
        BeanUtils.copyProperties(reciteHis,reciteHisOT);
        return reciteHisOT;
    }

    @GetMapping(value = "insertReciteHis")
    @CheckLogin
    public void insertReciteHis(PoemOT poemOT, HttpServletRequest request) {
        Integer memberId = (Integer) request.getAttribute("memberId");
        log.info("insertReciteHis start with memberId={}, poemOT = {}", memberId, poemOT);
        ReciteHis reciteHis = new ReciteHis();
        reciteHis.setMemberId(memberId);
        reciteHis.setPoemId(poemOT.getId());
        reciteHis.setAuthor(poemOT.getAuthor());
        reciteHis.setTitle(poemOT.getTitle());
        reciteHis.setDynasty(poemOT.getDynasty());
        reciteHis.setReciteDate(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        CurrentPoem currentPoem = currentPoemService.getOne(new QueryWrapper<CurrentPoem>()
                .eq("member_id", memberId)
                .eq("poem_id", poemOT.getId())
        );

        reciteHis.setViewDate(currentPoem.getHappenedDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        reciteHis.setStayDays(currentPoem.getStayDays());

        reciteHis.setCreateTime(new Date());
        reciteHis.setUpdateTime(reciteHis.getCreateTime());
        reciteHisService.save(reciteHis);
    }

}

