package com.zengchen.user.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zengchen.user.auth.CheckLogin;
import com.zengchen.user.client.service.UserCenterFeignClientService;
import com.zengchen.user.common.ReciteHisOT;
import com.zengchen.user.service.ReciteHisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Slf4j
public class UserCenterFeignClientController implements UserCenterFeignClientService {

    @Autowired
    private ReciteHisService reciteHisService;

    @Autowired
    HttpServletRequest httpServletRequest; // 是线程安全的

    @Override
    @CheckLogin
    public Page<ReciteHisOT> memberRctHisAno() {
        log.info("我是{},我被调用了",httpServletRequest.getRequestURL());
        Page<ReciteHisOT> page = new Page<>(1, 2);
        page.setRecords(reciteHisService.queryReciteHisPage(page,"3"));
        return page;
    }
}
