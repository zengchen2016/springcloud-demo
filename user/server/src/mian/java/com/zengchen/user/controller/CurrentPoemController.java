package com.zengchen.user.controller;


import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zengchen.user.auth.CheckLogin;
import com.zengchen.user.common.CurrentPoemOT;
import com.zengchen.user.entity.CurrentPoem;
import com.zengchen.user.service.CurrentPoemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zengchen123
 * @since 2019-11-04
 */
@RestController
@Slf4j
@RequestMapping("/currentPoem")
public class CurrentPoemController {

    @Autowired
    private CurrentPoemService currentPoemService;

    @GetMapping(value = "update")
    @CheckLogin
    public void update(CurrentPoemOT currentPoemOT) {
        log.info("update start with currentPoemOT={}", currentPoemOT);
        boolean result = currentPoemService.update(new UpdateWrapper<CurrentPoem>()
                .set("status",currentPoemOT.getStatus())
                .set("update_date", LocalDate.now())
                .eq("member_id",currentPoemOT.getMemberId())
                .eq("poem_id", currentPoemOT.getPoemId())
        );
        log.info("update end result={}",result);
    }
}

