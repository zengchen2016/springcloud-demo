package com.zengchen.content.config.ribbonrule;

import com.alibaba.nacos.api.naming.pojo.Instance;
import com.alibaba.nacos.client.naming.core.Balancer;

import java.util.List;

/**
 * <p>
 * 利用子类使用Balance的protected getHostByRandomWeight方法
 * </p>
 *
 * @author zengchen
 * @since 2019-08-11
 */
class ExtendsBalancer extends Balancer {
    public static Instance getInstanceByWeight(List<Instance> instances){
        return getHostByRandomWeight(instances);
    }

}
