package com.zengchen.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zengchen.user.common.UserInfoOT;
import com.zengchen.user.entity.Member;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
public interface MemberService extends IService<Member> {
    public List<String> selectTest();

    public UserInfoOT getMyPageUserInfoOT(Member member);
}
