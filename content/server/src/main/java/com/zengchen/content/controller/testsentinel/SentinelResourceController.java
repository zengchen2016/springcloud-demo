package com.zengchen.content.controller.testsentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.zengchen.content.sentinel.SentinelResourceBlockHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class SentinelResourceController {

    @GetMapping("/test-blockHandler")
    @SentinelResource(value = "blockHandler",
            blockHandler = "block",blockHandlerClass = SentinelResourceBlockHandler.class
//            ,fallback = "fallBack",fallbackClass = SentinelResourceFallbackHandler.class
             )
    public String testBlockHandler(String a,String b) throws InterruptedException {
        throw new IllegalArgumentException();
    }

}
