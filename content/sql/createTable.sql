CREATE TABLE `content`.`poem` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`title` VARCHAR (200) NULL COMMENT '标题',
	`dynasty` VARCHAR (20) NULL COMMENT '朝代',
	`author` VARCHAR (45) NULL COMMENT '作者',
	`orders` INT NULL COMMENT '发放顺序',
	`content` TEXT NULL COMMENT '内容',
	`only_content` TEXT NULL COMMENT '诗内容的纯汉字文本',
	`appreciation1` TEXT NULL COMMENT '译文',
    `appreciation2` TEXT NULL COMMENT '注释',
    `appreciation3` TEXT NULL COMMENT '赏析',
	`read_url` VARCHAR (200) NULL COMMENT '发音链接',
	`type` VARCHAR (1) NULL COMMENT '1：诗\n2：词\n3：古文',
	`mp3` VARCHAR (200) NULL COMMENT '音频名称',
	`createTime` DATETIME NULL,
	`updateTime` DATETIME NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `id_UNIQUE` (`id` ASC)
) ENGINE = INNODB DEFAULT CHARACTER
SET = utf8mb4 COLLATE = utf8mb4_general_ci AUTO_INCREMENT = 1 ROW_FORMAT = COMPACT;

CREATE TABLE `content`.`dynasty` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR (45) NULL,
	`order` VARCHAR (2) NULL COMMENT '排序',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `id_UNIQUE` (`id` ASC)
) ENGINE = INNODB DEFAULT CHARACTER
SET = utf8mb4 COLLATE = utf8mb4_general_ci AUTO_INCREMENT = 1 ROW_FORMAT = COMPACT;
