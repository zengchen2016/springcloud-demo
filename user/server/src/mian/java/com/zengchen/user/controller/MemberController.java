package com.zengchen.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zengchen.common.bean.ResponseVO;
import com.zengchen.common.enums.ResponseCode;
import com.zengchen.user.auth.CheckLogin;
import com.zengchen.user.common.UserInfoOT;
import com.zengchen.user.config.JwtConfig;
import com.zengchen.user.config.WXConfig;
import com.zengchen.user.entity.CurrentPoem;
import com.zengchen.user.entity.Member;
import com.zengchen.user.entity.ReciteHis;
import com.zengchen.user.service.CurrentPoemService;
import com.zengchen.user.service.MemberService;
import com.zengchen.user.service.ReciteHisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
@RestController
@Slf4j
@RequestMapping("/member")
//@RefreshScope
public class MemberController {

    @Autowired
    private MemberService memberService;

    @Autowired
    private ReciteHisService reciteHisService;

    @Autowired
    private WXConfig wxConfig;

    @Autowired
    private JwtConfig jwtConfig;

    @Autowired
    private CurrentPoemService currentPoemService;

    @GetMapping("/checkLogin")
    @CheckLogin
    public Object getMember(HttpServletRequest request) {
        Integer userId = (Integer)request.getAttribute("userId");
        log.info("从attribute里取出 userId = {}",userId.intValue());
        Member member = memberService.getOne(new QueryWrapper<Member>().eq("id", userId));
        log.info("根据userId查询用户信息，{}",member);
        return ResponseVO.success(member);
    }

    @PostMapping("/getMyPageData")
    @CheckLogin
    public Object getMyPageData(@RequestBody String requestBody, HttpServletRequest request) {
        Map<String,Object> responseData = new HashMap<>();
        try {
            Integer memberId = (Integer) request.getAttribute("memberId");
            log.info("getMyPageData with requestBody={}", requestBody);
            Member member = Optional.ofNullable(memberService.getOne(new QueryWrapper<Member>().eq("id", memberId)))
                    .orElseThrow(() -> new IllegalArgumentException("用户不存在 userId:"+ memberId));

            UserInfoOT userInfoOT = memberService.getMyPageUserInfoOT(member);

            responseData.put("userInfoOT",userInfoOT);

            return ResponseVO.success(responseData);
        }catch (IllegalArgumentException e) {
            log.error("token 不合法", e);
            return ResponseVO.fail(ResponseCode.ERROR_LOGGED_OUT);
        }catch (Exception e) {
            log.error("getMyPageData 异常", e);
            return ResponseVO.fail(ResponseCode.ERROR_SYS);
        }

    }



    @GetMapping("/getMyProperties")
    public Object testProperties() {
        return ResponseVO.success(jwtConfig);
    }
}

