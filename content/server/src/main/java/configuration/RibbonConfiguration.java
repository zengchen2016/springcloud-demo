package configuration;

import com.netflix.loadbalancer.IRule;
import com.zengchen.content.config.ribbonrule.NacosWeightedRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RibbonConfiguration {

    @Bean
    public IRule ribbonRule(){
//        return new RoundRobinRule();
        return new NacosWeightedRule();
//        return new NacosSameClusterWeightedRule();
//        return new NacosClusterMetadataWeightedRule();
    }
}
