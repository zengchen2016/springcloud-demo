package com.zengchen.content.controller.testsentinel;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/testSentinel")
public class FlowController {

    @Autowired
    FlowService flowService;

    @GetMapping("chain/a")
    public String chainA(){
        flowService.common();
        return "chain/a";
    }

    @GetMapping("chain/b")
    public String chainB(){
        flowService.common();
        return "chain/b";
    }

    @GetMapping("related/a")
    public String flowRelatedA(){
        log.info("我是被限流的资源......");
        return "related/a";
    }

    @GetMapping("related/b")
    public String flowRelatedB(){
        log.info("我是被关联资源，不是限流我......");
        return "related/b";
    }

    @GetMapping("QPSFlowControl")
    public String QPSFlowControl(){
        log.info("QPSFlowControl 我被执行了......");
        return "QPSFlowControl";
    }


}
