package com.zengchen.content.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zengchen.content.entity.Poem;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-03
 */
public interface PoemMapper extends BaseMapper<Poem> {

}
