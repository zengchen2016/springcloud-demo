package com.zengchen.user.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
@Getter
@Setter
@ToString
public class ReciteHisOT implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer memberId;

    private Integer poemId;
    /**
     * 派送诗词日期
     */
    private String viewDate;
    /**
     * 会背诵日期
     */
    private String reciteDate;
    /**
     * 与上次背诵间隔的天数
     */
    private Integer stayDays;

    private String dynasty;

    private String title;

    private String author;

}
