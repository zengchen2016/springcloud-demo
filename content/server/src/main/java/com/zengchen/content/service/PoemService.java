package com.zengchen.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zengchen.content.entity.Poem;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-03
 */
public interface PoemService extends IService<Poem> {

}
