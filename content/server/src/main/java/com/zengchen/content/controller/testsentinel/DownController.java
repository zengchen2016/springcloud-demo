package com.zengchen.content.controller.testsentinel;

import com.alibaba.csp.sentinel.Tracer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/down")
public class DownController {

    @GetMapping("rt/b")
    public String rtB() {
        try {
            log.info("rtB....");
            throw new RuntimeException("throw runtime ");
        } catch (Throwable t) {
            Tracer.trace(t);
        }
        return "rt/b";
    }

    @GetMapping("restful/{id}")
    public String restful(@PathVariable Integer id) {
        log.info("restful start with {}", id);
        return "" + id;
    }

    @GetMapping("restful/{id}/test")
    public String restfulTest(@PathVariable Integer id) {
        log.info("restfulTest start with {}", id);
        return "test " + id;
    }

}
