package com.zengchen.content.controller.testsentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class FlowService {

    @SentinelResource
    public void common(){
        log.info("common......");
    }
}
