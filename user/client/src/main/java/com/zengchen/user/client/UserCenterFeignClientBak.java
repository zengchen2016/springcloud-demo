package com.zengchen.user.client;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zengchen.user.common.ReciteHisOT;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "user-center")
public interface UserCenterFeignClientBak {

    /**
     * FeignClient的name + GetMapping的value
     * 相当于 http://user-center/reciteHis/testAno
     * 和RestTemplate里写的url一模一样
     * @return Page<ReciteHisOT>
     */
    @GetMapping(value = "/reciteHis/testAno")
    Page<ReciteHisOT> memberRctHisAno();
}
