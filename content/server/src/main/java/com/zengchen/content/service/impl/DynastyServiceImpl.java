package com.zengchen.content.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zengchen.content.entity.Dynasty;
import com.zengchen.content.mapper.DynastyMapper;
import com.zengchen.content.service.DynastyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-03
 */
@Service
public class DynastyServiceImpl extends ServiceImpl<DynastyMapper, Dynasty> implements DynastyService {

}
