package com.zengchen.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zengchen.user.entity.CurrentPoem;
import com.zengchen.user.mapper.CurrentPoemMapper;
import com.zengchen.user.service.CurrentPoemService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zengchen123
 * @since 2019-11-04
 */
@Service
public class CurrentPoemServiceImpl extends ServiceImpl<CurrentPoemMapper, CurrentPoem> implements CurrentPoemService {

}
