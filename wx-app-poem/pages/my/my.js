import common from '../../config/js/common';

//获取应用实例
const app = getApp()
var currentPage

Page({
  data: {
    motto: 'Hello World',
    userInfoOT: {
      "alreadyRecite": 0,
      "stayDays": 0,
      "registerDays": 0,
      "points": 0
    },
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    isLogin: false
  },
  onLoad: function () {
    let Pages = getCurrentPages()
    currentPage = Pages[Pages.length - 1]

    let token = wx.getStorageSync('token')
    if (token) {
      wx.request({
        url: common.user_center + "member/getMyPageData",
        method: 'POST',
        header: {
          'WX-TOKEN': token
        },
        data: {token: token},
        success(res) {
          let data = res.data;
          console.log("请求账户数据 后台返回")
          console.log(data)
          // 服务端登录注册成功
          if (data.code == 0) {
            let userInfoOT = data.data.userInfoOT
            currentPage.setData({
              userInfoOT: userInfoOT,
              isLogin: true
            })
          }
        },
        fail(res) {
          console.log("请求账户数据 服务调用异常：" + res)
        },
      })
    }
  },
  bindGetUserInfo: function (e) {
    console.log("getUserInfo")
    console.log(e)
    var userInfo = e.detail.userInfo;
    if (userInfo == undefined) {// 拒绝授权了
      console.log("拒绝授权");
      return;
    }
    // 成功之后调用login
    currentPage.userLogin(userInfo)
  },
  userLogin: function (userInfo) {
    wx.login({
      success(res) {
        if (res.code) {
          // TODO 封装
          wx.request({
            url: common.user_center + "login",
            method: 'POST',
            data: {
              code: res.code,
              userInfo: userInfo
            },
            success(res) {
              var data = res.data;
              console.log("login 后台返回")
              console.log(data)
              // 服务端登录注册成功
              if (data.code == 0) {
                let userInfoOT = data.data.userInfoOT
                currentPage.setData({
                  userInfoOT: userInfoOT,
                  isLogin: true
                })
                wx.setStorage({
                  key: "token",
                  data: userInfoOT.token
                })
              }
            }
          })
        } else {
          common.showTip('登录失败！' + res.errMsg);
        }
      }
    })
  },
  //历史记录
  bindToHis: function () {
    console.log("历史记录")
    if (this.data.isLogin) {
      console.log("已登录 ... ")
      wx.navigateTo({
        url: '../history/history'
      })
    } else {
      common.showTip("请先登录")
    }
  },
  //积分明细
  bindToPoints: function () {
    console.log("积分明细")
    if (this.data.isLogin) {
      console.log("已登录 ... ")
      wx.navigateTo({
        url: '../pointLog/pointLog'
      })
    } else {
      common.showTip("请先登录")
    }
  },
  onPullDownRefresh: function(){
    this.onLoad()
  },
  // 规则介绍
  toRules: function () {
    wx.navigateTo({
      url: '/pages/ruleIntro/ruleIntro'
    })
  },
  // 技术架构
  toTech: function () {
    wx.navigateTo({
      url: '/pages/techFramework/techFrame'
    })
  }
})
