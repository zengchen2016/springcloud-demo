package com.zengchen.user.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zengchen.user.common.ReciteHisOT;
import com.zengchen.user.entity.ReciteHis;
import com.zengchen.user.mapper.ReciteHisMapper;
import com.zengchen.user.service.ReciteHisService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
@Service
public class ReciteHisServiceImpl extends ServiceImpl<ReciteHisMapper, ReciteHis> implements ReciteHisService {

//    @Transactional(readOnly=true)
//    @Override
//    public Page<ReciteHisOT> selectUserListPage() {
//        Page<ReciteHisOT> page = new Page<>(1, 2);
//        return page.setRecords(this.baseMapper.selectUserListPage(page));
//    }

    @Override
    public List<ReciteHisOT> queryReciteHisPage(Page<ReciteHisOT> page,String  memberId) {
        return this.baseMapper.queryReciteHisPage(page,memberId);
    }

}
