package com.zengchen.content.utils;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class BoostrapPageUtils {
    /**
     * 查询条数
     */
    private String limit;
    /**
     * 从多少条开始查
     */
    private String offset;

    /**
     * 查询的当前页
     */
    private long currentPage;

    /**
     * 排序字段
     */
    private String sort;

    /**
     * 排序方式
     */
    private String order;

    public void setLimit(String limit) {
        this.limit = limit;
        countCurrenPage();
    }

    public void setOffset(String offset) {
        this.offset = offset;
        countCurrenPage();
    }

    private void countCurrenPage() {
        if(StringUtils.isNoneBlank(this.limit) && StringUtils.isNoneBlank(this.offset)){
            this.currentPage = Long.valueOf(this.offset)/Long.valueOf(this.limit) + 1;
        }
    }
}
