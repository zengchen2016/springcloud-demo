package com.zengchen.content.controller;/**
 * @program: content
 * @description: TestNacosConfigController
 * @author: zengchen
 * @create: 2019-09-13 15:02
 **/

import com.zengchen.common.bean.ResponseVO;
import com.zengchen.content.config.JwtConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: content
 * @description: TestNacosConfigController
 * @author: zengchen
 * @create: 2019-09-13 15:02
 **/
@RestController
@RefreshScope
public class TestNacosConfigController {

    @Autowired
    private JwtConfig jwtConfig;

    @Value("${jwt.secret}")
    private String jwtSecret;

    @GetMapping("/getMyProperties")
    public Object testProperties() {
        return ResponseVO.success(jwtConfig.toString() + " Controller的属性 jwtSecret=" + jwtSecret);
    }
}
