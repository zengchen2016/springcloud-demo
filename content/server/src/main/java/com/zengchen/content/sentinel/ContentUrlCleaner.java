package com.zengchen.content.sentinel;

import com.alibaba.csp.sentinel.adapter.servlet.callback.UrlCleaner;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Slf4j
public class ContentUrlCleaner implements UrlCleaner {
    @Override
    public String clean(String path) {
        log.info("ContentUrlCleaner start with {}", path);
        String[] paths = path.split("/");
        path = Arrays.stream(paths).map(string -> {
            if (NumberUtils.isNumber(string)) {
                return "{number}";
            }
            return string;
        }).reduce((a, b) -> {
            return a + "/" + b;
        }).orElse("");
        log.info("ContentUrlCleaner end with {}", path);
        return path;
    }
}
