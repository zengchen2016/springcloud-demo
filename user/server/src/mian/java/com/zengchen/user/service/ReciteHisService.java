package com.zengchen.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zengchen.user.common.ReciteHisOT;
import com.zengchen.user.entity.ReciteHis;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-04
 */
public interface ReciteHisService extends IService<ReciteHis> {

    public List<ReciteHisOT> queryReciteHisPage(Page<ReciteHisOT> page,String memberId);

}
