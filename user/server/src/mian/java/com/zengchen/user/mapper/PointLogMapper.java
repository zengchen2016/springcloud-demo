package com.zengchen.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zengchen.user.entity.PointLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zengchen123
 * @since 2019-11-02
 */
public interface PointLogMapper extends BaseMapper<PointLog> {

}
