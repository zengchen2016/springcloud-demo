CREATE SCHEMA `user` DEFAULT CHARACTER SET utf8mb4;

CREATE TABLE `user`.`member`
(
  `id`         INT          NOT NULL AUTO_INCREMENT,
  `wx_id`      VARCHAR(45)  NULL COMMENT '微信id',
  `nickname`   VARCHAR(200) NULL COMMENT '昵称',
  `head_url`   VARCHAR(200) NULL COMMENT '头像图片地址',
  `sex`        VARCHAR(1)   NULL COMMENT '1：男\n2：女\n0：未知',
  `points`     INT          NULL COMMENT '积分',
  `createTime` DATETIME     NULL,
  `updateTime` DATETIME     NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
) ENGINE = INNODB
  DEFAULT CHARACTER
    SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  AUTO_INCREMENT = 1
  ROW_FORMAT = COMPACT;

CREATE TABLE `user`.`reciteHis`
(
  `id`          INT          NOT NULL AUTO_INCREMENT,
  `member_id`   INT          NOT NULL COMMENT '用户ID',
  `poem_id`     INT          NOT NULL COMMENT '诗词ID',
  `author`      VARCHAR(45)  NULL COMMENT '作者',
  `title`       VARCHAR(200) NULL COMMENT '标题',
  `dynasty`     VARCHAR(20)  NULL COMMENT '朝代',
  `view_date`   VARCHAR(10)  NOT NULL COMMENT '派送诗词日期',
  `recite_date` VARCHAR(10)  NOT NULL COMMENT '会背诵日期',
  `stay_days`   INT          NOT NULL COMMENT '与上次背诵间隔的天数',
  `createTime`  DATETIME     NULL,
  `updateTime`  DATETIME     NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
) ENGINE = INNODB
  DEFAULT CHARACTER
    SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  AUTO_INCREMENT = 1
  ROW_FORMAT = COMPACT;

CREATE TABLE `user`.`pointLog`
(
  `id`         INT        NOT NULL AUTO_INCREMENT,
  `member_id`  INT        NOT NULL COMMENT '用户ID',
  `type`       VARCHAR(1) NOT NULL COMMENT '1：增加\n2：减少',
  `amount`     INT        NOT NULL COMMENT '发生数量',
  `remarks`    VARCHAR(100) COMMENT '说明',
  `createTime` DATETIME   NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
) ENGINE = INNODB
  DEFAULT CHARACTER
    SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  AUTO_INCREMENT = 1
  ROW_FORMAT = COMPACT;

CREATE TABLE `user`.`currentPoem`
(
  `id`            INT      NOT NULL AUTO_INCREMENT,
  `member_id`     INT      NOT NULL COMMENT '用户ID',
  `poem_id`       INT      NOT NULL COMMENT '诗词',
  `happened_date` DATETIME NOT NULL COMMENT '发放日期',
  `stay_days`     INT COMMENT '停滞天数',
  `status`     varchar(1) DEFAULT 'N' COMMENT '是否背诵过,N：没有；Y：已背',
  `update_date` DATE NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
) ENGINE = INNODB
  DEFAULT CHARACTER
    SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  AUTO_INCREMENT = 1
  ROW_FORMAT = COMPACT;