package com.zengchen.content.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zengchen.content.entity.Dynasty;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zengchen123
 * @since 2019-08-03
 */
public interface DynastyService extends IService<Dynasty> {

}
