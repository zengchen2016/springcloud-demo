package com.zengchen.content.common;

import lombok.Data;

import java.util.Date;

@Data
public class PoemOT {

    private Integer id;
    /**
     * 标题
     */
    private String title;
    /**
     * 朝代
     */
    private String dynasty;
    /**
     * 作者
     */
    private String author;
    /**
     * 内容
     */
    private String content;
    /**
     * 译文
     */
    private String appreciation1;
    /**
     * 注释
     */
    private String appreciation2;
    /**
     * 赏析
     */
    private String appreciation3;
    /**
     * 1：诗
     2：词
     3：古文
     */
    private String type;

    // 发放顺序
    private Integer orders;
    /**
     * 音频名称
     */
    private String mp3;

    private Date createTime;

    private Date updateTime;
}
