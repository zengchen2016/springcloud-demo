package com.zengchen.content.feignclient;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zengchen.content.common.PoemOT;
import com.zengchen.content.sentinel.SentinelFeignFallbackFactory;
import com.zengchen.user.common.CurrentPoemOT;
import com.zengchen.user.common.ReciteHisOT;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//@FeignClient(name = "user-center",configuration = UserCenterFeignClientConfiguration.class)
//@FeignClient(name = "user-center", fallback = SentinelFeignFallbackHandler.class)
@FeignClient(name = "user-center", fallbackFactory = SentinelFeignFallbackFactory.class)
public interface UserCenterFeignClient
//        extends UserCenterFeignClientService
{

    /**
     * FeignClient的name + GetMapping的value
     * 相当于 http://user-center/reciteHis/testAno
     * 和RestTemplate里写的url一模一样
     * @return Page
     */
    @GetMapping(value = "/reciteHis/testAno")
    Page<ReciteHisOT> memberRctHisAno();


    @GetMapping(value = "/reciteHis/getReciteHis")
    ReciteHisOT getReciteHis(@RequestParam("poemId") Integer poemId);

    @GetMapping(value = "/reciteHis/insertReciteHis")
    void insertReciteHis(@SpringQueryMap PoemOT poemOT);

    @GetMapping(value = "/currentPoem/update")
    void updateCurrentPoem(@SpringQueryMap CurrentPoemOT currentPoemOT);


}
