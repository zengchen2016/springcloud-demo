package com.zengchen.content.controller.testsentinel;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zengchen.user.common.ReciteHisOT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Slf4j
@RequestMapping("/restTemplate")
public class RestTemplateController {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("test")
    public Object test() {
        String targetUrl = "http://user-center/reciteHis/testAno";
        Page<ReciteHisOT> page = restTemplate.getForObject(targetUrl, Page.class);
        return page;
    }
}
