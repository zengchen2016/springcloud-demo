package com.zengchen.content.sentinel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SentinelResourceFallbackHandler {

    public static String fallBack(String a,String b,Throwable e){
        log.warn("fallBack method",e);
        return "出现异常了";
    }
}
