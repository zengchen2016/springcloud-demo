package com.zengchen.content.sentinel;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zengchen.content.common.PoemOT;
import com.zengchen.content.feignclient.UserCenterFeignClient;
import com.zengchen.user.common.CurrentPoemOT;
import com.zengchen.user.common.ReciteHisOT;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SentinelFeignFallbackFactory implements FallbackFactory<UserCenterFeignClient> {

    @Override
    public UserCenterFeignClient create(Throwable throwable) {
        return new UserCenterFeignClient() {
            @Override
            public Page<ReciteHisOT> memberRctHisAno() {
                log.warn("memberRctHisAno FallbackFactory");
                return null;
            }

            @Override
            public ReciteHisOT getReciteHis(Integer poemId) {
                log.warn("getReciteHis FallbackFactory");
                return null;
            }

            @Override
            public void insertReciteHis(PoemOT poemOT) {
                log.warn("insertReciteHis FallbackFactory");
            }

            @Override
            public void updateCurrentPoem(CurrentPoemOT currentPoemOT) {
                log.warn("updateCurrentPoem FallbackFactory");
            }
        };
    }
}
