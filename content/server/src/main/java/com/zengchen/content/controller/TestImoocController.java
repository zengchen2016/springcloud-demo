package com.zengchen.content.controller;

import com.zengchen.content.feignclient.TestImoocFeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestImoocController {

    @Resource
    TestImoocFeignClient testImoocFeignClient;

    @GetMapping(value = "getImooc")
    public String imoocIndex(){
        return this.testImoocFeignClient.index();
    }
}
