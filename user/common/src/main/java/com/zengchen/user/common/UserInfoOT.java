package com.zengchen.user.common;

import lombok.Data;

/**
 * @program: user
 * @description: UserInfoOT
 * @author: zengchen
 * @create: 2019-11-01 16:39
 **/
@Data
public class UserInfoOT {

    private String token;
    private Integer userId;
    private Integer points;
    private String userName;
    private String headUrl;
    private Integer alreadyRecite;
    private Integer stayDays;
    private long registerDays;
}
