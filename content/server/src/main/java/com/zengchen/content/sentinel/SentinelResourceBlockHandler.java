package com.zengchen.content.sentinel;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityException;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeException;
import com.alibaba.csp.sentinel.slots.block.flow.FlowException;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowException;
import com.alibaba.csp.sentinel.slots.system.SystemBlockException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SentinelResourceBlockHandler {

    public static  String block(String a,String b, BlockException e){
        String errorMsg = "Unknown";
        // 根据异常类型区分违反哪种规则
        if(e instanceof FlowException){// 限流异常
            errorMsg = "被限流规则阻挡";
        }else if(e instanceof DegradeException){ // 降级异常
            errorMsg = "被降级规则阻挡";
        }else if(e instanceof ParamFlowException){ // 热点参数异常
            errorMsg = "被热点参数规则阻挡";
        }else if(e instanceof SystemBlockException){ // 系统规则异常
            errorMsg = "被系统规则阻挡";
        }else if(e instanceof AuthorityException){ // 认证异常
            errorMsg = "被授权规则阻挡";
        }
        log.warn("block method，{}",errorMsg ,e);
        return errorMsg;
    }


}
