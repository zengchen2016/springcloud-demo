package com.zengchen.content.config;

import com.zengchen.content.config.ribbonrule.NacosWeightedRule;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Configuration;

@Configuration
//@RibbonClients(defaultConfiguration = RibbonConfiguration.class)
@RibbonClient(name = "user-center",configuration = NacosWeightedRule.class)
//@RibbonClient(name = "user-center")
public class RibbonClientConfig {
}
