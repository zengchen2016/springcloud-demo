package com.zengchen.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zengchen.common.bean.ResponseVO;
import com.zengchen.common.enums.ResponseCode;
import com.zengchen.user.auth.CheckLogin;
import com.zengchen.user.entity.PointLog;
import com.zengchen.user.service.PointLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zengchen123
 * @since 2019-11-02
 */
@RestController
@RequestMapping("/pointLog")
@Slf4j
public class PointLogController {

    @Autowired
    private PointLogService pointLogService;

    @GetMapping("/getPointLog")
    @CheckLogin
    public Object getPointLog(@ModelAttribute Page<PointLog> page, HttpServletRequest request) {
        Map<String,Object> responseData = new HashMap<>();
        try {
            Integer memberId = (Integer) request.getAttribute("memberId");
            log.info("getPointLog start with current={},size={},memberId={}", page.getCurrent(),page.getSize(), memberId);

            IPage<PointLog> dataPage = pointLogService.page(page, new QueryWrapper<PointLog>()
                    .eq("member_id", memberId)
                    .orderByDesc("createTime")
                    .orderByAsc("id") // 测试的时候使用
            );
            responseData.put("dataPage",dataPage);
            log.info("getPointLog end with success!");
            return ResponseVO.success(responseData);
        }catch (Exception e) {
            log.error("getPointLog end with Exception", e);
            return ResponseVO.fail(ResponseCode.ERROR_SYS);
        }

    }
}

