package com.zengchen.user.common;/**
 * @program: user
 * @description: CurrentPoemOT
 * @author: zengchen
 * @create: 2019-11-10 16:27
 **/

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @program: user
 * @description: CurrentPoemOT
 * @author: zengchen
 * @create: 2019-11-10 16:27
 **/
@Data
public class CurrentPoemOT {

    private Integer id;
    /**
     * 用户ID
     */
    private Integer memberId;
    /**
     * 诗词
     */
    private Integer poemId;
    /**
     * 发放日期
     */
    private LocalDateTime happenedDate;
    /**
     * 停滞天数
     */
    private Integer stayDays;

    private String status;

    private LocalDate updateDate;
}
