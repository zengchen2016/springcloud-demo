package com.zengchen.content.handle;

import com.zengchen.common.bean.ErrorCodeConts;
import com.zengchen.common.bean.ResponseVO;
import com.zengchen.common.enums.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandle {

    @ExceptionHandler(value = RuntimeException.class)
    public Object error(RuntimeException e){
        log.warn("发生 RuntimeException 异常",e);
        if(ErrorCodeConts.UNAUTHORIZED.equals(e.getMessage())){
//            return new ResponseEntity<>(
//                    ErrorBody.builder()
//                            .code(HttpStatus.UNAUTHORIZED.value()+"")
//                            .msg("未登录")
//                            .build()
//                    ,HttpStatus.UNAUTHORIZED);
            return ResponseVO.fail(ResponseCode.ERROR_LOGGED_OUT);
        }
        return ResponseVO.fail(9999,"未作处理异常！");
    }

}
