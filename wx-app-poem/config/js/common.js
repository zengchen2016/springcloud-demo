//var gateway = 'http://192.168.199.147:8040';
//var gateway = 'http://192.168.0.133:8040';
//var gateway = 'http://106.13.54.74:8040';
var domain = 'https://www.ttbsc.cn';
var gateway = domain + '/api/';

var mp3_url = domain + '/static-web/mp3/';
var img_url = domain + '/static-web/images/';
var user_center = gateway+ "/user-center/";
var content_center = gateway + "/content-center/";

var openConfirm = function (content, confirmText, cancelText,firstFunc,secondFunc) {
  wx.showModal({
    title: '提示',
    content: content,
    confirmText: confirmText,
    cancelText: cancelText,
    success: function (res) {
      if (res.confirm) {
        if (firstFunc){
          firstFunc()
        }
      } else {
        if (secondFunc) {
          secondFunc()
        }
      }
    }
  });
}

var showTip = function (content,succFunc) {
  wx.showModal({
    content: content,
    showCancel: false,
    success: function (res) {
      if (succFunc){
        succFunc()
      }
    }
  });
}

module.exports = {
  user_center: user_center,
  content_center: content_center,
  gateway: gateway,
  openConfirm: openConfirm,
  showTip: showTip,
  mp3_url: mp3_url,
  img_url: img_url,
}