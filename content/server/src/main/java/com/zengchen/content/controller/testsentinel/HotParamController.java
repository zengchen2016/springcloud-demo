package com.zengchen.content.controller.testsentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/hot")
public class HotParamController {

    @GetMapping("/qiguai")
    // 热点参数规则必须加上这个注解，把参数纳入监测
    // 但是加上这个注解，又不能被UrlBlockHandler处理，无法美化错误
    // 只能用注解的blockHandler或者fallback 属性来美化错误
    @SentinelResource(value = "basic")
    public String qiguai(String a,String b) {
        log.info("qiguai......");
        return a + ":" + b;
    }

    @GetMapping("senior")
    @SentinelResource("senior")
    public String senior(String a, String b) {
        log.info("senior......");
        return a + ":" + b;
    }

}
